// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB2_WorldPawn_generated_h
#error "WorldPawn.generated.h already included, missing '#pragma once' in WorldPawn.h"
#endif
#define CS378_LAB2_WorldPawn_generated_h

#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_SPARSE_DATA
#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPerformInteraction);


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPerformInteraction);


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_EVENT_PARMS
#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_CALLBACK_WRAPPERS
#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWorldPawn(); \
	friend struct Z_Construct_UClass_AWorldPawn_Statics; \
public: \
	DECLARE_CLASS(AWorldPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(AWorldPawn)


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAWorldPawn(); \
	friend struct Z_Construct_UClass_AWorldPawn_Statics; \
public: \
	DECLARE_CLASS(AWorldPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(AWorldPawn)


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWorldPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWorldPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldPawn(AWorldPawn&&); \
	NO_API AWorldPawn(const AWorldPawn&); \
public:


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldPawn(AWorldPawn&&); \
	NO_API AWorldPawn(const AWorldPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWorldPawn)


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PawnStaticMeshComponent() { return STRUCT_OFFSET(AWorldPawn, PawnStaticMeshComponent); } \
	FORCEINLINE static uint32 __PPO__HitBoxComponent() { return STRUCT_OFFSET(AWorldPawn, HitBoxComponent); } \
	FORCEINLINE static uint32 __PPO__CameraComponent() { return STRUCT_OFFSET(AWorldPawn, CameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AWorldPawn, CameraBoom); }


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_14_PROLOG \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_EVENT_PARMS


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_RPC_WRAPPERS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_CALLBACK_WRAPPERS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_INCLASS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_CALLBACK_WRAPPERS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_INCLASS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_WorldPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB2_API UClass* StaticClass<class AWorldPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab2_Source_CS378_Lab2_WorldPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
