// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB2_CollisionActor_generated_h
#error "CollisionActor.generated.h already included, missing '#pragma once' in CollisionActor.h"
#endif
#define CS378_LAB2_CollisionActor_generated_h

#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_SPARSE_DATA
#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_RPC_WRAPPERS
#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACollisionActor(); \
	friend struct Z_Construct_UClass_ACollisionActor_Statics; \
public: \
	DECLARE_CLASS(ACollisionActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(ACollisionActor)


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACollisionActor(); \
	friend struct Z_Construct_UClass_ACollisionActor_Statics; \
public: \
	DECLARE_CLASS(ACollisionActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(ACollisionActor)


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACollisionActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACollisionActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollisionActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollisionActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollisionActor(ACollisionActor&&); \
	NO_API ACollisionActor(const ACollisionActor&); \
public:


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollisionActor(ACollisionActor&&); \
	NO_API ACollisionActor(const ACollisionActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollisionActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollisionActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACollisionActor)


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(ACollisionActor, MeshComponent); } \
	FORCEINLINE static uint32 __PPO__TriggerComponent() { return STRUCT_OFFSET(ACollisionActor, TriggerComponent); }


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_11_PROLOG
#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_RPC_WRAPPERS \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_INCLASS \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_INCLASS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_CollisionActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB2_API UClass* StaticClass<class ACollisionActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab2_Source_CS378_Lab2_CollisionActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
