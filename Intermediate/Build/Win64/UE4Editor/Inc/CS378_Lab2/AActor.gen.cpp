// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CS378_Lab2/AActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAActor() {}
// Cross Module References
	CS378_LAB2_API UClass* Z_Construct_UClass_AAActor_NoRegister();
	CS378_LAB2_API UClass* Z_Construct_UClass_AAActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CS378_Lab2();
// End Cross Module References
	void AAActor::StaticRegisterNativesAAActor()
	{
	}
	UClass* Z_Construct_UClass_AAActor_NoRegister()
	{
		return AAActor::StaticClass();
	}
	struct Z_Construct_UClass_AAActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CS378_Lab2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AActor.h" },
		{ "ModuleRelativePath", "AActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAActor_Statics::ClassParams = {
		&AAActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAActor, 4289551096);
	template<> CS378_LAB2_API UClass* StaticClass<AAActor>()
	{
		return AAActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAActor(Z_Construct_UClass_AAActor, &AAActor::StaticClass, TEXT("/Script/CS378_Lab2"), TEXT("AAActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
