// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB2_AActor_generated_h
#error "AActor.generated.h already included, missing '#pragma once' in AActor.h"
#endif
#define CS378_LAB2_AActor_generated_h

#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_SPARSE_DATA
#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_RPC_WRAPPERS
#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAActor(); \
	friend struct Z_Construct_UClass_AAActor_Statics; \
public: \
	DECLARE_CLASS(AAActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(AAActor)


#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAActor(); \
	friend struct Z_Construct_UClass_AAActor_Statics; \
public: \
	DECLARE_CLASS(AAActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab2"), NO_API) \
	DECLARE_SERIALIZER(AAActor)


#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAActor(AAActor&&); \
	NO_API AAActor(const AAActor&); \
public:


#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAActor(AAActor&&); \
	NO_API AAActor(const AAActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAActor)


#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_PRIVATE_PROPERTY_OFFSET
#define CS378_Lab2_Source_CS378_Lab2_AActor_h_9_PROLOG
#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_RPC_WRAPPERS \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_INCLASS \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab2_Source_CS378_Lab2_AActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_SPARSE_DATA \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_INCLASS_NO_PURE_DECLS \
	CS378_Lab2_Source_CS378_Lab2_AActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB2_API UClass* StaticClass<class AAActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab2_Source_CS378_Lab2_AActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
