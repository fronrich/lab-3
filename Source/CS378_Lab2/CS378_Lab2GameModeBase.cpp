// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab2GameModeBase.h"
#include "WorldPawn.h"

ACS378_Lab2GameModeBase::ACS378_Lab2GameModeBase()
{
	// set default pawn class to our character class
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/MyWorldPawn.MyWorldPawn_C'"));

    if (GEngine)
    {
        if (pawnBPClass.Object)
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
            UClass* pawnBP = (UClass*)pawnBPClass.Object;
            DefaultPawnClass = pawnBP;
        }
        else
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
            DefaultPawnClass = AWorldPawn::StaticClass();
        }
    }
}