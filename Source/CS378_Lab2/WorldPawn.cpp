// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldPawn.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/CollisionProfile.h"
#include "Engine/StaticMesh.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "CollisionActor.h"
#include "Components/BoxComponent.h"
// #include "TriggerActor.h"

const FName AWorldPawn::MoveForwardBinding("MoveForward");
const FName AWorldPawn::MoveRightBinding("MoveRight");

const FName AWorldPawn::InteractBinding("Interact");

// Sets default values
AWorldPawn::AWorldPawn()
{
 	// Create the mesh component
	PawnStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	PawnStaticMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when pawn does
	CameraBoom->TargetArmLength = 1200.f;
	CameraBoom->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm


	// Movement
	MoveSpeed = 1000.0f;

}

// Called when the game starts or when spawned
void AWorldPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldPawn::Tick(float DeltaSeconds)
{
	// Super::Tick(DeltaSeconds);

	// Find movement direction
	const float ForwardValue = GetInputAxisValue(MoveForwardBinding);
	const float RightValue = GetInputAxisValue(MoveRightBinding);

	// Clamp max size so that (X=1, Y=1) doesn't cause faster movement in diagonal directions
	const FVector MoveDirection = FVector(ForwardValue, RightValue, 0.f).GetClampedToMaxSize(1.0f);

	// Calculate  movement
	const FVector Movement = MoveDirection * MoveSpeed * DeltaSeconds;

	// If non-zero size, move this actor
	if (Movement.SizeSquared() > 0.0f)
	{
		const FRotator NewRotation = Movement.Rotation();
		FHitResult Hit(1.f);
		RootComponent->MoveComponent(Movement, NewRotation, true, &Hit);
		
		if (Hit.IsValidBlockingHit())
		{
			const FVector Normal2D = Hit.Normal.GetSafeNormal2D();
			const FVector Deflection = FVector::VectorPlaneProject(Movement, Normal2D) * (1.f - Hit.Time);
			RootComponent->MoveComponent(Deflection, NewRotation, true);
		}
	}

}

// Called to bind functionality to input
void AWorldPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAction(InteractBinding, IE_Pressed, this, &AWorldPawn::InteractPressed);

	// set up gameplay key bindings
	PlayerInputComponent->BindAxis(MoveForwardBinding);
	PlayerInputComponent->BindAxis(MoveRightBinding);

}

void AWorldPawn::PerformInteraction()
{
	// UE4 Keeps crashing in this function for some reason. I think it
	// has something to do with these declarations but this assignment is 4 days late and
	// I'm finally throwing in the towel
	TArray<AActor *> OverlappingActors;
    PawnStaticMeshComponent->GetOverlappingActors(OverlappingActors);
	// for (AActor *actor : OverlappingActors)
	// {
	// 	if(GEngine) {
	// 	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("CollisionActor destroy"));
	// 	}
	// }
	if(GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("CollisionActor destroy"));
		}
	
	// for(AActor * actor : OverlappingActors)
	// {
	// 	if(actor->IsA(ACollisionActor::StaticClass()))
	// 	{
	// 		actor->Destroy();
	// 	}
	// }
}

