// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActorBox.h"

// add spherecomponent
#include "Components/SphereComponent.h"

// Sets default values
AMyActorBox::AMyActorBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// create components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	TriggerComponent = CreateAbstractDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	TriggerComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

}

// Called when the game starts or when spawned
void AMyActorBox::BeginPlay()
{
	Super::BeginPlay();
	GetTriggerComponent()->OnComponentBeginOverlap.AddDynamic(this, &AMyActorBox::BeginOverlap);
}

// Called every frame
void AMyActorBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// created
void AMyActorBox::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult) {
	if (OtherActor->IsA(APawn::StaticClass())) {
		this->Destroy();
	}
}