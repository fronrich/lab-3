// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollisionActor.generated.h"

class USphereComponent;

UCLASS()
class CS378_LAB2_API ACollisionActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACollisionActor();

	FORCEINLINE UStaticMeshComponent * GetMeshComponent() const {return MeshComponent; }
	FORCEINLINE USphereComponent * GetTriggerComponent() const { return TriggerComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) 
		UStaticMeshComponent * MeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) 
		USphereComponent* TriggerComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
