// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "WorldPawn.generated.h"

class UBoxComponent;

UCLASS()
class CS378_LAB2_API AWorldPawn : public APawn
{
	GENERATED_BODY()

	// /* The mesh component */
	// UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	// class UStaticMeshComponent* PawnStaticMeshComponent;

	// /** The camera */
	// UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	// class UCameraComponent* CameraComponent;

	// /** Camera boom positioning the camera above the character */
	// UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	// class USpringArmComponent* CameraBoom;
public:
	// Sets default values for this pawn's properties
	AWorldPawn();

	/* The speed our ship moves around the level */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

	static const FName MoveForwardBinding;
    static const FName MoveRightBinding;
    static const FName InteractBinding;

    UFUNCTION(BlueprintCallable)
    void PerformInteraction();

    UFUNCTION(BlueprintImplementableEvent)
    void InteractPressed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UStaticMeshComponent *PawnStaticMeshComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UBoxComponent *HitBoxComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UCameraComponent *CameraComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    USpringArmComponent *CameraBoom;

public:	
    // Called every frame
    virtual void Tick(float DeltaSeconds) override;


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/* Returns MeshComponent subobject */
    FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return PawnStaticMeshComponent; }
    /* Returns CameraComponent subobject */
    FORCEINLINE class UCameraComponent *GetCameraComponent() const { return CameraComponent; }
    /* Returns CameraBoom subobject */
    FORCEINLINE class USpringArmComponent *GetCameraBoom() const { return CameraBoom; }

};
